package com.jay.simplempegdashplayer;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.analytics.AnalyticsCollector;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Log;
import com.jay.simplempegdashplayer.data.MovieInfos;
import com.jay.simplempegdashplayer.debugView.DebugView;

import java.util.List;

public class PlayerActivity extends Activity {
    public static final String MDASH_URL = "http://demo-dash-vod.zahs.tv/exodus/manifest.mpd";
    public static final String HELLO_MSG = "Hi Zattoo! This is Jay Jeon :)";
    SurfaceView playerView;

    MovieInfos infos;
    DefaultBandwidthMeter bandwidthMeter;
    DataSource.Factory dsFactory;
    SimpleExoPlayer exoPlayer;
    DefaultTrackSelector trackSelector;
    MediaSource mediaSource;

    DebugView debugView;
    ProgressBar progressBar;
    TextView informationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        playerView = findViewById(R.id.player_view);
        debugView = findViewById(R.id.debugView);
        progressBar = findViewById(R.id.progress_circular);
        informationView = findViewById(R.id.information);
        Intent intent = getIntent();
        infos = intent.getParcelableExtra("next_battle");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Title: " + infos.title, Toast.LENGTH_LONG).show();
        initPlayer();
    }

    private void initPlayer() {
        initBandwidthMeter();
        if (bandwidthMeter != null)
            initDataSourceFactory();
        initExoPlayer();
        setDataSource();
        resumePlayer();
    }

    private void initBandwidthMeter() {
        bandwidthMeter = new DefaultBandwidthMeter.Builder(this)
                .setInitialBitrateEstimate("KR")
                .build();
        bandwidthMeter.addEventListener(new Handler(Looper.getMainLooper()), new BandwidthMeter.EventListener() {
            @Override
            public void onBandwidthSample(int elapsedMs, long bytesTransferred, long bitrateEstimate) {
                if (debugView != null)
                    runOnUiThread(() -> {
                        debugView.draw(elapsedMs, bitrateEstimate, bytesTransferred);
                    });
            }
        });
    }

    private void initDataSourceFactory() {
        dsFactory = new DefaultHttpDataSource.Factory()
                .setTransferListener(bandwidthMeter)
                .setAllowCrossProtocolRedirects(true)
                .setUserAgent("Zatto-testApp");
    }

    private void initExoPlayer() {
        trackSelector = new DefaultTrackSelector(this, new AdaptiveTrackSelection.Factory());


        trackSelector.setParameters(new DefaultTrackSelector.ParametersBuilder(this)
                .setAllowVideoNonSeamlessAdaptiveness(false)
                .setExceedRendererCapabilitiesIfNecessary(true)
                .setExceedAudioConstraintsIfNecessary(true)
                .setAllowMultipleAdaptiveSelections(true)
                /*.setTunnelingEnabled(true)*/.build());

        RenderersFactory renderersFactory = new DefaultRenderersFactory(this);
        exoPlayer = new SimpleExoPlayer.Builder(this, renderersFactory)
                .setTrackSelector(trackSelector)
                .setHandleAudioBecomingNoisy(true)
                .setLooper(Looper.getMainLooper())
                .setUseLazyPreparation(false)
                .build();
        exoPlayer.setVideoSurface(playerView.getHolder().getSurface());
        exoPlayer.addAnalyticsListener(new EventLogger(trackSelector));
        exoPlayer.addListener(new Player.Listener() {
            @Override
            public void onRenderedFirstFrame() {
                informationView.setText(getTrackInfos());
            }

            @Override
            public void onPlaybackStateChanged(int state) {
                if (state == Player.STATE_BUFFERING) {
                    runOnUiThread(() -> {
                        informationView.setText(HELLO_MSG);
                        progressBar.setVisibility(View.VISIBLE);
                    });
                } else {
                    runOnUiThread(() -> {
                        informationView.setText(getTrackInfos());
                        progressBar.setVisibility(View.GONE);
                    });
                }
            }

            @Override
            public void onCues(List<Cue> cues) {

            }

            @Override
            public void onMetadata(Metadata metadata) {

            }
        });
        exoPlayer.setForegroundMode(true);
    }

    private String getTrackInfos() {
        StringBuilder sb = new StringBuilder();
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo == null) return "";
        int rendererCount = mappedTrackInfo.getRendererCount();
        for (int rendererIndex = 0; rendererIndex < rendererCount; rendererIndex++) {
            TrackGroupArray rendererTrackGroups = mappedTrackInfo.getTrackGroups(rendererIndex);
            TrackSelection trackSelection = exoPlayer.getCurrentTrackSelections().get(rendererIndex);
            if (rendererTrackGroups.length != 0) {
                sb.append("  " + mappedTrackInfo.getRendererName(rendererIndex) + " [\n");
                for (int groupIndex = 0; groupIndex < rendererTrackGroups.length; groupIndex++) {
                    TrackGroup trackGroup = rendererTrackGroups.get(groupIndex);
                    sb.append("    Group:" + groupIndex + ", adaptive_supported=" + " [\n");
                    for (int trackIndex = 0; trackIndex < trackGroup.length; trackIndex++) {
                        String formatSupport =
                                C.getFormatSupportString(
                                        mappedTrackInfo.getTrackSupport(rendererIndex, groupIndex, trackIndex));
                        sb.append("      "
                                + " Track:"
                                + trackIndex
                                + ", "
                                + Format.toLogString(trackGroup.getFormat(trackIndex))
                                + ", supported="
                                + formatSupport + "\n");
                    }
                    sb.append("    ]\n");
                }
                sb.append("  ]\n");
            }
        }
        return sb.toString();
    }

    private void setDataSource() { // we know we will use only mpeg dash streaming
        if (MDASH_URL.toLowerCase().contains(".mpd") && exoPlayer != null) {
            mediaSource = new DashMediaSource.Factory(dsFactory)
                    .createMediaSource(
                            new MediaItem.Builder()
                                    .setUri(Uri.parse(MDASH_URL))
                                    .build()
                    );
            exoPlayer.setMediaSource(mediaSource);
        }
    }

    private void resumePlayer() {
        if (!exoPlayer.isPlaying()) {
            exoPlayer.prepare();
            exoPlayer.setPlayWhenReady(true);
            Log.d("jayjay", "resume Player");
        }
    }

    private void pausePlayer() {
        if (exoPlayer.isPlaying()) {
            exoPlayer.setPlayWhenReady(false);
            Log.d("jayjay", "pause Player");
        }
    }

    private void stopPlayer() {
        exoPlayer.setPlayWhenReady(false);
        exoPlayer.stop(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
