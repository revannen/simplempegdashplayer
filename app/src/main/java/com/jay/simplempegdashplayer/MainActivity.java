package com.jay.simplempegdashplayer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.jay.simplempegdashplayer.data.MovieInfos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private String TMDB_API_URL = "https://api.themoviedb.org/3/trending/all/day?api_key=283a74de43294779f5511e635add0406";
    private String TMDB_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";
    MovieListAdapter movieAdapter;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridView = findViewById(R.id.listview);
        movieAdapter = new MovieListAdapter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestMasterNode();
    }

    private void requestMasterNode() {
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, TMDB_API_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject resultObj = new JSONObject(response);
                            int page = resultObj.optInt("page"); //get page just in case... :)
                            JSONArray resultList = resultObj.optJSONArray("results");
                            for (int i = 0; i < resultList.length(); i++) {
                                JSONObject object = resultList.getJSONObject(i);
                                MovieInfos tmp = new MovieInfos(object.optString("release_date"), object.optInt("adult")
                                        , object.optString("backdrop_path"), JSonArray2IntArray(object.optJSONArray("genre_ids"))
                                        , object.optInt("vote_count"), object.optInt("id")
                                        , object.optString("original_title"), object.getString("poster_path")
                                        , object.optString("title"), object.optInt("video")
                                        , object.optDouble("vote_average"), object.optString("original_language")
                                        , object.optString("overview"), object.optDouble("popularity"), object.optString("media_type"));
                                movieAdapter.addInfos(tmp);
                            }
                            gridView.setAdapter(movieAdapter);
                            gridView.setOnItemClickListener((parent, view, position, id) -> {
                                MovieInfos item = (MovieInfos) movieAdapter.getItem(position);
                                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                                intent.putExtra("next_battle", item);
                                Log.d("jay", "before title: " + item.title);
                                startActivityForResult(intent, 1);
                            });
                            gridView.invalidateViews();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "volley error: " + error.toString(), Toast.LENGTH_SHORT);
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Log.d("jayjay", "resultCode: " + resultCode);
        }
    }

    public static int[] JSonArray2IntArray(JSONArray jsonArray){
        int[] intArray = new int[jsonArray.length()];
        for (int i = 0; i < intArray.length; ++i) {
            intArray[i] = jsonArray.optInt(i);
        }
        return intArray;
    }

    class MovieListAdapter extends BaseAdapter {
        final public ArrayList<MovieInfos> movieInfosArray = new ArrayList<>();
        Context context;
        LayoutInflater inflater;

        public MovieListAdapter(Context context) {
            this.context = context;
            inflater = LayoutInflater.from(this.context);
        }

        public void addInfos(MovieInfos infos) {
            movieInfosArray.add(infos);
        }
        @Override
        public int getCount() {
            return movieInfosArray.size();
        }

        @Override
        public Object getItem(int position) {
            return movieInfosArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.listview_item, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            MovieInfos info = movieInfosArray.get(position);
            holder.tvTitle.setText(info.originalTitle);

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.progress_animation)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(getApplicationContext())
                    .load(loadImageFromUri(info))
                    .apply(options)
                    .into(holder.image);
            return convertView;
        }

        private Uri loadImageFromUri(MovieInfos info) {
            return Uri.parse(TMDB_IMAGE_BASE_URL + info.posterPath);
        }

        private class ViewHolder {
            TextView tvTitle;
            ImageView image;

            public ViewHolder(View item) {
                this.tvTitle = item.findViewById(R.id.title);
                this.image = item.findViewById(R.id.bg_image);
            }
        }
    }
}