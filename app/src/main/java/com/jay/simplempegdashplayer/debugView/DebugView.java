package com.jay.simplempegdashplayer.debugView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.exoplayer2.util.Log;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DebugView extends ScrollView {
    Context context;
    LineChart speed, health, network;

    public DebugView(Context context) {
        super(context);
        initViews();
    }

    public DebugView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public DebugView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    public void initViews() {
        context = getContext();
        int paddingSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, context.getResources().getDisplayMetrics());
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250f, context.getResources().getDisplayMetrics());
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        setFillViewport(true);
        setFocusable(false);
        LayoutParams layoutParams = new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        layoutParams.gravity = Gravity.BOTTOM | Gravity.END;
        setLayoutParams(layoutParams);

        LinearLayout containerLayout = new LinearLayout(context);
        LinearLayout.LayoutParams containerParams = new LinearLayout.LayoutParams(width, height);
        containerLayout.setBackgroundColor(0xC0000000);
        containerLayout.setOrientation(LinearLayout.VERTICAL);
        containerLayout.setPadding(paddingSize, 0, paddingSize, paddingSize);
        containerLayout.setLayoutParams(containerParams);

        speed = new LineChart(context);
        health = new LineChart(context);
        network = new LineChart(context);
        initChartViews();
        containerLayout.addView(speed);
        containerLayout.addView(health);
        containerLayout.addView(network);
        addView(containerLayout);
    }

    private void initChartViews() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                , (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60f, context.getResources().getDisplayMetrics()));
        speed.setLayoutParams(layoutParams);
        health.setLayoutParams(layoutParams);
        network.setLayoutParams(layoutParams);
        initCharts(speed, health, network);
    }

    private LineDataSet createDataSetForChart(int color) {
        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(color);
        set.setDrawFilled(true);
        set.setDrawCircles(false);
        set.setLineWidth(0);
        set.setFillAlpha(65);
        set.setFillColor(color);
        set.setDrawValues(false);
        return set;
    }

    private void initCharts(LineChart... charts) {
        for (LineChart chart : charts) {
            XAxis speed_xl = chart.getXAxis();
            YAxis speed_yl = chart.getAxisRight();
            YAxis speed_rightAxis = chart.getAxisLeft();
            LineData speed_data = new LineData();

            chart.getDescription().setEnabled(false);
            chart.getLegend().setTextColor(Color.WHITE);
            chart.setTouchEnabled(false);
            chart.setDragEnabled(false);
            chart.setScaleEnabled(false);
            chart.setDrawGridBackground(false);
            chart.setPinchZoom(false);
            chart.setBackgroundColor(Color.TRANSPARENT);
            chart.getXAxis().setEnabled(false);
            speed_xl.setDrawLabels(false);
            speed_xl.setDrawGridLines(false);
            speed_xl.setAvoidFirstLastClipping(true);
            speed_xl.setPosition(XAxis.XAxisPosition.BOTTOM);
            speed_xl.setEnabled(true);
            speed_yl.setDrawLabels(false);
            speed_yl.setDrawGridLines(false);
            speed_yl.setEnabled(true);
            speed_rightAxis.setEnabled(false);
            chart.setData(speed_data);
        }
    }

    private void updateStatChart(LineChart chart, float value, int color, String formattedValue) {
        LineData data = chart.getData();
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            if (set == null) {
                set = createDataSetForChart(color);
                data.addDataSet(set);
            }
            set.setLabel(formattedValue);
            data.addEntry(new Entry(set.getEntryCount(), value), 0);
            data.notifyDataChanged();
            chart.notifyDataSetChanged();
            chart.setVisibleXRangeMaximum(180);
            chart.moveViewToX(data.getEntryCount());
        }
    }

    public void draw(long bufferedDurationMS, long bitrate, long bytesDownloaded) {
        String bitrateStr = getFormattedDouble(bitrate / Math.pow(10, 3), 1);
        updateStatChart(network, (float) (bytesDownloaded / Math.pow(10, 3)), Color.CYAN, "BytesDownloaded : " + humanReadableBytesCount(bytesDownloaded, true));
        updateStatChart(health, (float) (bufferedDurationMS), ColorTemplate.getHoloBlue(), "Buffer Health : " + bufferedDurationMS + "ms");
        updateStatChart(speed, Float.parseFloat(bitrateStr), Color.LTGRAY, "Bitrate : " + humanReadableBytesCount(bitrate, true) + "ps");
    }

    @SuppressLint("DefaultLocale")
    private String humanReadableBytesCount(long bytes, boolean si) {
        int unit = !si ? 1000 : 1024;
        if (bytes < unit) {
            return bytes + "kb";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp -1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private String getFormattedDouble(double value, int prec) {
        String pattern;
        if (prec <= 1) {
            pattern = "0";
        } else if (prec <= 2) {
            pattern = "00";
        } else {
            pattern = "000";
        }

        return new DecimalFormat("#0." + pattern).format(value);
    }
}
