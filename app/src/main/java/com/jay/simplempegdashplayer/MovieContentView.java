package com.jay.simplempegdashplayer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class MovieContentView extends LinearLayout {
    TextView title;
    ImageView background;

    public MovieContentView(Context context) {
        super(context);
        init(context);
    }

    public MovieContentView(Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listview_item, this, true);

        title = findViewById(R.id.title);
        background = findViewById(R.id.bg_image);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }
}
