package com.jay.simplempegdashplayer.data;


import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.Arrays;

public class MovieInfos implements Parcelable {
    public String releaseDate;
    public int adult;
    public String backDropPath;
    public int[] genreIds;
    public int voteCount;
    public int id;
    public String originalTitle;
    public String posterPath;
    public String title;
    public int video;
    public double vote_average;
    public String originalLanguage;
    public String overview;
    public double popularity;
    public String mediaType;

    public MovieInfos(String releaseDate, int adult, String backDropPath, int[] genreIds
            , int voteCount, int id, String originalTitle, String posterPath, String title
            , int video, double vote_average, String originalLanguage, String overview
            , double popularity, String mediaType) {
        this.releaseDate = releaseDate;
        this.adult = adult;
        this.backDropPath = backDropPath;
        this.genreIds = genreIds;
        this.voteCount = voteCount;
        this.id = id;
        this.originalTitle = originalTitle;
        this.posterPath = posterPath;
        this.title = title;
        this.video = video;
        this.vote_average = vote_average;
        this.originalLanguage = originalLanguage;
        this.overview = overview;
        this.popularity = popularity;
        this.mediaType = mediaType;
    }

    protected MovieInfos(Parcel in) {
        releaseDate = in.readString();
        backDropPath = in.readString();
        originalTitle = in.readString();
        posterPath = in.readString();
        title = in.readString();
        originalLanguage = in.readString();
        overview = in.readString();
        mediaType = in.readString();
        adult = in.readInt();
        video = in.readInt();
        voteCount = in.readInt();
        id = in.readInt();
        genreIds = in.createIntArray();
        vote_average = in.readDouble();
        popularity = in.readDouble();
    }

    public static final Creator<MovieInfos> CREATOR = new Creator<MovieInfos>() {
        @Override
        public MovieInfos createFromParcel(Parcel in) {
            return new MovieInfos(in);
        }

        @Override
        public MovieInfos[] newArray(int size) {
            return new MovieInfos[size];
        }
    };

    @Override
    public String toString() {
        return "MovieInfos{" +
                "releaseDate='" + releaseDate + '\'' +
                ", adult=" + adult +
                ", backDropPath='" + backDropPath + '\'' +
                ", genreIds=" + Arrays.toString(genreIds) +
                ", voteCount=" + voteCount +
                ", id=" + id +
                ", originalTitle='" + originalTitle + '\'' +
                ", posterPath='" + posterPath + '\'' +
                ", title='" + title + '\'' +
                ", video=" + video +
                ", vote_average=" + vote_average +
                ", originalLanguage='" + originalLanguage + '\'' +
                ", overview='" + overview + '\'' +
                ", popularity=" + popularity +
                ", mediaType='" + mediaType + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(releaseDate);
        dest.writeString(backDropPath);
        dest.writeString(originalTitle);
        dest.writeString(posterPath);
        dest.writeString(title);
        dest.writeString(originalLanguage);
        dest.writeString(overview);
        dest.writeString(mediaType);
        dest.writeInt(adult);
        dest.writeInt(video);
        dest.writeInt(voteCount);
        dest.writeInt(id);
        dest.writeIntArray(genreIds);
        dest.writeDouble(vote_average);
        dest.writeDouble(popularity);
    }
}
